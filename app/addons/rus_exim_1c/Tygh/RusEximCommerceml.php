<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh;

use Tygh\Registry;

class RusEximCommerceml
{
    public static $url;

    public static function xmlCheckValidate($file_path, $cml)
    {
        $t_commerceml = $t_product = false;
        $xml_validate = true;
        $xml = new \XMLReader();
        if (file_exists($file_path) && ($xml->open($file_path)) && (filesize($file_path) != 0)) {
            while (@$xml->read()) {
                if($xml->nodeType == \XMLReader::END_ELEMENT){
                    if ($xml->name === $cml['commerceml']) {
                        $t_commerceml = true;
                    }
                    if (($xml->name === $cml['catalog']) || ($xml->name === $cml['packages'])) {
                        $t_product = true;
                    }
                }
            }

            if (!$t_commerceml || !$t_product) {
                $xml_validate = false;
            }
        }

        return $xml_validate;
    }
}
