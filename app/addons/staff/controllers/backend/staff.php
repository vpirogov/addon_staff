<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']  == 'POST') {

    fn_trusted_vars('staff_member_data');
    $suffix = '';

    //
    // Delete staff
    //
    if ($mode == 'm_delete') {
        
        foreach ($_REQUEST['staff_member_ids'] as $v) {
            fn_delete_staff_by_id($v);
        }

        $suffix = '.manage';
    }

    //
    // Add/edit staff
    //
    if ($mode == 'update') {           
    
        $staff_member_id = fn_staff_get_update_staff_member_data($_REQUEST['staff_member_data'], $_REQUEST['staff_member_id'], DESCR_SL);

        $suffix = ".update?staff_member_id=$staff_member_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_member_id'])) {
            fn_delete_staff_by_id($_REQUEST['staff_member_id']);
        }

        $suffix = '.manage';
    }
    if ($mode == 'update_status') {

        $staff_data = db_get_row("SELECT * FROM ?:staff WHERE staff_id = ?i", $_REQUEST['id']);
        if (!empty($staff_data)) {
            $result = db_query("UPDATE ?:staff SET status = ?s WHERE staff_id = ?i", $_REQUEST['status'], $_REQUEST['id']);
            if ($result) {
                fn_set_notification('N', __('notice'), __('status_changed'));
            } else {
                fn_set_notification('E', __('error'), __('error_status_not_changed'));
                Tygh::$app['ajax']->assign('return_status', $staff_data['status']);
            }
        }

        exit;
    }
    
    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'update') {
    
    $staff_member = fn_get_staff_data($_REQUEST['staff_member_id'], DESCR_SL);

    if (empty($staff_member)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
    
    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

    Tygh::$app['view']->assign('staff_member', $staff_member);

} elseif ($mode == 'manage' || $mode == 'picker') {

    list($staff, ) = fn_get_staff(array(), DESCR_SL);
    Tygh::$app['view']->assign('staff', $staff);
}
