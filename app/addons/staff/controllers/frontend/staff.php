<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

//
// View staff list
//

if ($mode == 'view_all') {   

    fn_add_breadcrumb(__('staffs'));

    list($staff) = fn_get_staff($_REQUEST);

    Tygh::$app['view']->assign('staff', $staff);

}


//
// View staff details
//
if ($mode == 'view') {  

    fn_add_breadcrumb(__('staffs'));  

    $staff= fn_get_staff_data($_REQUEST['staff_id'] );
    
    Tygh::$app['view']->assign('staff', $staff);
}

