<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

//
// Get staff
//
function fn_get_staff($params = array(), $lang_code = CART_LANGUAGE)
{
    $default_params = array(
        'items_per_page' => 0,
    );

    $params = array_merge($default_params, $params);

    $sortings = array(
        'position' => '?:staff.position',        
        'name' => '?:staff_descriptions.first_name',
    );

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:staff.staff_id IN (?n)', explode(',', $params['item_ids']));
    }

    

    fn_set_hook('get_staff', $params, $condition, $sorting, $limit, $lang_code);

    $fields = array (
        '?:staff.staff_id',       
        '?:staff.email',
        '?:staff.position',
        '?:staff.status',
        '?:staff_descriptions.staff_id',
        '?:staff_descriptions.first_name',
        '?:staff_descriptions.last_name',
        '?:staff_descriptions.function',
        '?:staff_descriptions.description',
        '?:staff_descriptions.lang_code',        
        '?:staff_images.staff_image_id',
    );

    

    $staff = db_get_hash_array(
        "SELECT ?p FROM ?:staff " .
        "LEFT JOIN ?:staff_descriptions ON ?:staff_descriptions.staff_id = ?:staff.staff_id AND ?:staff_descriptions.lang_code = ?s" .        
        " LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id " .         
        " WHERE 1 ?p ?p ?p",
        'staff_id', implode(", ", $fields), $lang_code, $condition, $sorting, $limit
    );

    $staff_image_ids = fn_array_column($staff, 'staff_image_id');
    
    $images = fn_get_image_pairs($staff_image_ids, 'promo', 'M', true, false, $lang_code);
    foreach ($staff as $staff_id => $staff_member) {
        $staff[$staff_id]['main_pair'] = !empty($images[$staff_member['staff_image_id']]) ? reset($images[$staff_member['staff_image_id']]) : array();
    }

    fn_set_hook('get_staff_post', $staff, $params);

    return array($staff, $params);
}

//
// Get specific staff data
//
function fn_get_staff_data($staff_id, $lang_code = CART_LANGUAGE)
{
    // Unset all SQL variables
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:staff.staff_id',       
        '?:staff.email',
        '?:staff.position',
        '?:staff.status',
        '?:staff_descriptions.staff_id',
        '?:staff_descriptions.first_name',
        '?:staff_descriptions.last_name',
        '?:staff_descriptions.function',
        '?:staff_descriptions.description',   
        '?:staff_descriptions.lang_code',     
        '?:staff_images.staff_image_id',
    );

    
    $joins[] = db_quote("LEFT JOIN ?:staff_descriptions ON ?:staff_descriptions.staff_id = ?:staff.staff_id AND ?:staff_descriptions.lang_code = ?s", $lang_code);
    $joins[] = db_quote("LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id");

    $condition = db_quote("WHERE ?:staff.staff_id = ?i", $staff_id);
    $condition .= (AREA == 'A') ? '' : " AND ?:staff.status IN ('A', 'H') ";

    /**
     * Prepare params for staff data SQL query
     *
     * @param int   $staff_id Banner ID
     * @param str   $lang_code Language code
     * @param array $fields    Fields list
     * @param array $joins     Joins list
     * @param str   $condition Conditions query
     */
    fn_set_hook('get_staff_data', $staff_id, $lang_code, $fields, $joins, $condition);

    $staff = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:staff " . implode(" ", $joins) ." $condition");

    if (!empty($staff)) {
        $staff['main_pair'] = fn_get_image_pairs($staff['staff_image_id'], 'promo', 'M', true, false, $lang_code);
    }

    /**
     * Post processing of staff data
     *
     * @param int   $staff_id Banner ID
     * @param str   $lang_code Language code
     * @param array $staff    Banner data
     */
    fn_set_hook('get_staff_data_post', $staff_id, $lang_code, $staff);

    return $staff;
}

/**
 * Hook for deleting store staff
 *
 * @param int $company_id Company id
 */
function fn_staff_delete_company(&$company_id)
{
    if (fn_allowed_for('ULTIMATE')) {
        $bannser_ids = db_get_fields("SELECT staff_id FROM ?:staff WHERE company_id = ?i", $company_id);

        foreach ($bannser_ids as $staff_id) {
            fn_delete_staff_by_id($staff_id);
        }
    }
}

/**
 * Deletes staff and all related data
 *
 * @param int $staff_id Staff identificator
 */
function fn_delete_staff_by_id($staff_id)
{
    if (!empty($staff_id) && fn_check_company_id('staff', 'staff_id', $staff_id)) {
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
        db_query("DELETE FROM ?:staff_descriptions WHERE staff_id = ?i", $staff_id);

        fn_set_hook('delete_staff', $staff_id);

        Block::instance()->removeDynamicObjectData('staff', $staff_id);

        $staff_images_ids = db_get_fields("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $staff_id);

        foreach ($staff_images_ids as $staff_image_id) {
            fn_delete_image_pairs($staff_image_id, 'promo');
        }

        db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
    }
}

function fn_staff_need_image_update()
{
    if (!empty($_REQUEST['file_staff_main_image_icon']) && array($_REQUEST['file_staff_main_image_icon'])) {
        $image_staff = reset ($_REQUEST['file_staff_main_image_icon']);

        if ($image_staff == 'staff_main') {
            return false;
        }
    }

    return true;
}

function fn_staff_get_update_staff_member_data($data, $staff_id, $lang_code = DESCR_SL)
{
    
    if (!empty($staff_id)) {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $staff_id);
        db_query("UPDATE ?:staff_descriptions SET ?u WHERE staff_id = ?i AND lang_code = ?s", $data, $staff_id, $lang_code);

        $staff_image_id = fn_get_staff_image_id($staff_id, $lang_code);
        $staff_image_exist = !empty($staff_image_id);
        $staff_is_multilang = Registry::get('addons.staff.staff_multilang') == 'Y';
        $image_is_update = fn_staff_need_image_update();

        if ($staff_is_multilang) {
            if ($staff_image_exist && $image_is_update) {
                fn_delete_image_pairs($staff_image_id, 'promo');
                db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i ", $staff_id);
                $staff_image_exist = false;
            }
        } else {
            if (isset($data['url'])) {
                db_query("UPDATE ?:staff_descriptions SET url = ?s WHERE staff_id = ?i", $data['url'], $staff_id);
            }
        }

        if ($image_is_update && !$staff_image_exist) {
            $staff_image_id = db_query("INSERT INTO ?:staff_images (staff_id) VALUE(?i)", $staff_id);
        }
        $pair_data = fn_attach_image_pairs('staff_main', 'promo', $staff_image_id, $lang_code);

        if (!$staff_is_multilang && !$staff_image_exist) {
            fn_staff_image_all_links($staff_id, $pair_data, $lang_code);
        }

    } else {
        $staff_id = $data['staff_id'] = db_query("REPLACE INTO ?:staff ?e", $data);

        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:staff_descriptions ?e", $data);
        }

        if (fn_staff_need_image_update()) {
            $staff_image_id = db_get_next_auto_increment_id('staff_images');
            $pair_data = fn_attach_image_pairs('staff_main', 'promo', $staff_image_id, $lang_code);
            if (!empty($pair_data)) {
                $data_staff_image = array(
                    'staff_image_id' => $staff_image_id,
                    'staff_id'       => $staff_id,
                    'lang_code'       => $lang_code
                );

                db_query("INSERT INTO ?:staff_images ?e", $data_staff_image);                
            }
        }
    }

    return $staff_id;
}

function fn_staff_image_all_links($staff_id, $pair_data, $main_lang_code = DESCR_SL)
{
    if (!empty($pair_data)) {
        $pair_id = reset($pair_data);

        $lang_codes = Languages::getAll();
        unset($lang_codes[$main_lang_code]);

        foreach ($lang_codes as $lang_code => $lang_data) {
            $_staff_image_id = db_query("INSERT INTO ?:staff_images (staff_id) VALUE(?i)", $staff_id);
            fn_add_image_link($_staff_image_id, $pair_id);
        }
    }
}

function fn_get_staff_image_id($staff_id, $lang_code = DESCR_SL)
{
    return db_get_field("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
}

//
// Get staff name
//
function fn_get_staff_name($staff_id, $lang_code = CART_LANGUAGE)
{
    if (!empty($staff_id)) {
        return db_get_field("SELECT staff FROM ?:staff_descriptions WHERE staff_id = ?i AND lang_code = ?s", $staff_id, $lang_code);
    }

    return false;
}

function fn_staff_delete_image_pre($image_id, $pair_id, $object_type)
{
    if ($object_type == 'promo') {
        $staff_data = db_get_row("SELECT staff_id, staff_image_id FROM ?:staff_images INNER JOIN ?:images_links ON object_id = staff_image_id WHERE pair_id = ?i", $pair_id);


        $staff_image_ids = db_get_fields("SELECT object_id FROM ?:images_links WHERE image_id = ?i AND object_type = 'promo'", $image_id);

        if (!empty($staff_image_ids)) {
            db_query("DELETE FROM ?:staff_images WHERE staff_image_id IN (?a)", $staff_image_ids);
            db_query("DELETE FROM ?:images_links WHERE object_id IN (?a)", $staff_image_ids);
        }
 
    }
}

function fn_staff_clone($staff, $lang_code)
{
    foreach ($staff as $staff) {
        if (empty($staff['main_pair']['pair_id'])) {
            continue;
        }

        $data_staff_image = array(
            'staff_id' => $staff['staff_id'],
            'lang_code' => $lang_code
        );
        $staff_image_id = db_query("REPLACE INTO ?:staff_images ?e", $data_staff_image);
        fn_add_image_link($staff_image_id, $staff['main_pair']['pair_id']);
    }
}

function fn_staff_update_language_post($language_data, $lang_id, $action)
{
    if ($action == 'add') {
        list($staff) = fn_get_staff(array(), DEFAULT_LANGUAGE);
        fn_staff_clone($staff, $language_data['lang_code']);
    }
}

function fn_staff_delete_languages_post($lang_ids, $lang_codes, $deleted_lang_codes)
{
    foreach ($deleted_lang_codes as $lang_code) {
        list($staff) = fn_get_staff(array(), $lang_code);

        foreach ($staff as $staff) {
            if (empty($staff['main_pair']['pair_id'])) {
                continue;
            }
            fn_delete_image($staff['main_pair']['image_id'], $staff['main_pair']['pair_id'], 'promo');
        }
    }
}

