{if $staff_member}
    {assign var="id" value=$staff_member.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{assign var="allow_save" value=$staff_member|fn_allow_save_object:"staff"}

{** staff section **}

{assign var="b_type" value=$staff_member.type|default:"G"}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  {if !$allow_save} cm-hide-inputs{/if}" name="staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="staff_member_id" value="{$id}" />

{capture name="tabsbox"}
<div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_first_name" class="control-label cm-required">{__("first_name")}</label>
        <div class="controls">
        <input type="text" name="staff_member_data[first_name]" id="elm_staff_member_first_name" value="{$staff_member.first_name}" size="5"/></div>
    </div>       

    <div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_last_name" class="control-label cm-required">{__("last_name")}</label>
        <div class="controls">
        <input type="text" name="staff_member_data[last_name]" id="elm_staff_member_last_name" value="{$staff_member.last_name}" size="5"/></div>
    </div>

    <div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_function" class="control-label cm-required">{__("function")}</label>
        <div class="controls">
        <input type="text" name="staff_member_data[function]" id="elm_staff_member_function" value="{$staff_member.function}" size="5"/></div>
    </div>

    <div class="control-group cm-no-hide-input">
    <label for="elm_staff_member_description" class="control-label">{__("description")}</label>
        <div class="controls">
        <textarea name="staff_member_data[description]" id="elm_staff_member_description" cols="25" rows="8" class="cm-wysiwyg input-large">{$staff_member.description}</textarea>
        </div>
    </div>

    <div class="control-group {if $b_type != "G"}hidden{/if}" id="staff_member_graphic">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_main" image_object_type="promo" image_pair=$staff_member.main_pair image_object_id=$id no_detailed=true hide_titles=true hide_alt=true}
        </div>
    </div>        

    <div class="control-group">
        <label for="elm_staff_member_email" class="control-label cm-email">{__("email")}</label>
        <div class="controls">
            <input type="text" name="staff_member_data[email]" id="elm_staff_member_email" value="{$staff_member.email}" size="3"/>
        </div>
    </div>

    <div class="control-group">
        <label for="elm_staff_member_position" class="control-label">{__("position")}</label>
        <div class="controls">
            <input type="number" name="staff_member_data[position]" id="elm_staff_member_position" value="{$staff_member.position|default:0}" size="5"/>
        </div>
    </div>  

    {include file="common/select_status.tpl" input_name="staff_member_data[status]" id="elm_staff_member_status" obj_id=$id obj=$staff_member hidden=true}
    
</div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
    {else}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}
    
</form>

{/capture}



{if !$id}
    {assign var="title" value=__("staff.new_staff_member")}
{else}
    {assign var="title" value="{__("staff.editing_staff_member")}: `$staff_member.first_name` `$staff_member.last_name`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}

{** staff_member section **}
