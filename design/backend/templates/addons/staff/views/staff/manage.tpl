
{** staff section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="staff_form" class=" " enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />

{if $staff}
<table class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">
    {include file="common/check_items.tpl"}</th>
    <th width="10%" >{__("position")}</th>
    <th width="40%" >{__("staff_name")}</th>    
    <th width="6%">&nbsp;</th>
    <th width="10%" class="right">{__("status")}</th>
</tr>
</thead>
{foreach from=$staff item='staff_member'}

<tr class="cm-row-status-{$staff_member.status|lower}">
  
    <td class="left">
        <input type="checkbox" name="staff_member_ids[]" value="{$staff_member.staff_id}" class="cm-item "/>
    </td>
    <td class="{$no_hide_input}">
        <a class="row-status" href="{"staff.update?staff_member_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.position|default:0}</a>
    </td>    
    <td class="row-status">{if $staff_member.first_name || $staff_member.last_name}<a href="{"staff.update?staff_member_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.last_name} {$staff_member.first_name}</a>{else}-{/if}</td>    
    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff.update?staff_member_id=`$staff_member.staff_id`"}</li>
            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?staff_member_id=`$staff_member.staff_id`"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>    

    <td class="right">
        {if $staff_member.status == "A"}
            <label class="radio inline" for="{$id}_{$staff_member.status}_p"><input type="hidden" name="{$input_name}" id="{$id}_{$obj_id|default:0}_p" checked="checked" value="A"/>{__("active")}</label>
        {/if}

        {if $staff_member.status == "H"}
            <label class="radio inline" for="{$id}_{$staff_member.status}_p"><input type="hidden" name="{$input_name}" id="{$id}_{$obj_id|default:0}_p" checked="checked" value="H"/>{__("hidden")}</label>
        {/if}

        {if $staff_member.status == "D"}
            <label class="radio inline" for="{$id}_{$staff_member.status}_p"><input type="hidden" name="{$input_name}" id="{$id}_{$obj_id|default:0}_p" checked="checked" value="D"/>{__("deactive")}</label>
        {/if}
        
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {capture name="main_tools_list"}
        {if $staff}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="staff_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.main_tools_list}    
{/capture}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("add_staff_member") icon="icon-plus"}
{/capture}

</form>
{/capture}
{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}

{** ad section **}
