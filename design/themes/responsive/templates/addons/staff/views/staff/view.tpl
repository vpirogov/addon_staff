{if $staff.main_pair}
    <p><div class="ty-control-group cm-reload " style="height: 150px; width: 200px; overflow: hidden;">{include file="common/image.tpl" obj_id=$index images=$staff.main_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}
    </div></p>
{/if}

{if $staff.first_name}
    <div class="ty-control-group cm-reload" id="first_name_update">
        <label class="ty-control-group__label" id="first_name">{__("first_name")}:</label>
        <span class="ty-control-group__item" id="first_name_value">{$staff.first_name}</span>
    <!--first_name_update--></div>
{/if}

{if $staff.last_name}
    <div class="ty-control-group cm-reload" id="last_name_update">
        <label class="ty-control-group__label" id="last_name">{__("last_name")}:</label>
        <span class="ty-control-group__item" id="last_name_value">{$staff.last_name}</span>
    <!--last_name_update--></div>
{/if}

{if $staff.function}
    <div class="ty-control-group cm-reload" id="function_update">
        <label class="ty-control-group__label" id="function">{__("function")}:</label>
        <span class="ty-control-group__item" id="function_value">{$staff.function}</span>
    <!--function_update--></div>
{/if}

{if $staff.email}
    <div class="ty-control-group cm-reload" id="email_update">
        <label class="ty-control-group__label" id="email">{__("email")}:</label>
        <span class="ty-control-group__item" id="email_value">{$staff.email}</span>
    <!--email_update--></div>
{/if}

{if $staff.description}
    <div class="ty-control-group cm-reload" id="description_update">
        <label class="ty-control-group__label" id="description">{__("description")}:</label>
        <span class="ty-control-group__item" id="description_value">{$staff.description|truncate:25000:"...":true nofilter}</span>
    <!--description_update--></div>
{/if}

{if $staff.position}
    <div class="ty-control-group cm-reload" id="position_update">
        <label class="ty-control-group__label" id="position">{__("position")}:</label>
        <span class="ty-control-group__item" id="position_value">{$staff.position}</span>
    <!--position_update--></div>
{/if}

