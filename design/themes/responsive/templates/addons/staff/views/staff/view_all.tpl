{if $staff}
{$size = 4}
{split data=$staff size=$size assign="splitted_filter" preverse_keys=true}

<div class="ty-features-all">
{foreach from=$splitted_filter item="group"}
    {foreach from=$group item="ranges" key="index"}

    <div class="ty-features-all__group ty-column6">
        {if $ranges}
        <p><a href="{"staff.view?staff_id=`$ranges.staff_id`"|fn_url}" class="ty-features-all__list-a">
            {if $ranges.main_pair}
                <div class="ty-cart-content__image " style="height: 150px; width: 200px; overflow: hidden;">{include file="common/image.tpl" obj_id=$index images=$ranges.main_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}
                </div>
            {/if}
        </p>
        <p>{$ranges.first_name|truncate:12:"...":true nofilter}   {$ranges.last_name|truncate:12:"...":true nofilter}</p><p>{$ranges.function|truncate:12:"...":true nofilter}</p></a>
        {/if}
    </div>
    {/foreach}
{/foreach}
</div>
{/if}

