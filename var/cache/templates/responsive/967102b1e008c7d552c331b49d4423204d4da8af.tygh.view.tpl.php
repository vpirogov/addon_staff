<?php /* Smarty version Smarty-3.1.21, created on 2016-01-18 14:29:24
         compiled from "/var/www/html/cscart_standart/design/themes/responsive/templates/addons/staff/views/staff/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19027030125698f8b7a00b86-54235786%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '967102b1e008c7d552c331b49d4423204d4da8af' => 
    array (
      0 => '/var/www/html/cscart_standart/design/themes/responsive/templates/addons/staff/views/staff/view.tpl',
      1 => 1453116562,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19027030125698f8b7a00b86-54235786',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5698f8b7a8e388_10267857',
  'variables' => 
  array (
    'runtime' => 0,
    'staff' => 0,
    'index' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5698f8b7a8e388_10267857')) {function content_5698f8b7a8e388_10267857($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/var/www/html/cscart_standart/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/var/www/html/cscart_standart/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('first_name','last_name','function','email','description','position','first_name','last_name','function','email','description','position'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['staff']->value['main_pair']) {?>
    <p><div class="ty-control-group cm-reload " style="height: 150px; width: 200px; overflow: hidden;"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['index']->value,'images'=>$_smarty_tpl->tpl_vars['staff']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

    </div></p>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['first_name']) {?>
    <div class="ty-control-group cm-reload" id="first_name_update">
        <label class="ty-control-group__label" id="first_name"><?php echo $_smarty_tpl->__("first_name");?>
:</label>
        <span class="ty-control-group__item" id="first_name_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--first_name_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['last_name']) {?>
    <div class="ty-control-group cm-reload" id="last_name_update">
        <label class="ty-control-group__label" id="last_name"><?php echo $_smarty_tpl->__("last_name");?>
:</label>
        <span class="ty-control-group__item" id="last_name_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--last_name_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['function']) {?>
    <div class="ty-control-group cm-reload" id="function_update">
        <label class="ty-control-group__label" id="function"><?php echo $_smarty_tpl->__("function");?>
:</label>
        <span class="ty-control-group__item" id="function_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['function'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--function_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['email']) {?>
    <div class="ty-control-group cm-reload" id="email_update">
        <label class="ty-control-group__label" id="email"><?php echo $_smarty_tpl->__("email");?>
:</label>
        <span class="ty-control-group__item" id="email_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['email'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--email_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['description']) {?>
    <div class="ty-control-group cm-reload" id="description_update">
        <label class="ty-control-group__label" id="description"><?php echo $_smarty_tpl->__("description");?>
:</label>
        <span class="ty-control-group__item" id="description_value"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['staff']->value['description'],25000,"...",true);?>
</span>
    <!--description_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['position']) {?>
    <div class="ty-control-group cm-reload" id="position_update">
        <label class="ty-control-group__label" id="position"><?php echo $_smarty_tpl->__("position");?>
:</label>
        <span class="ty-control-group__item" id="position_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['position'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--position_update--></div>
<?php }?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/staff/views/staff/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/staff/views/staff/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['staff']->value['main_pair']) {?>
    <p><div class="ty-control-group cm-reload " style="height: 150px; width: 200px; overflow: hidden;"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['index']->value,'images'=>$_smarty_tpl->tpl_vars['staff']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

    </div></p>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['first_name']) {?>
    <div class="ty-control-group cm-reload" id="first_name_update">
        <label class="ty-control-group__label" id="first_name"><?php echo $_smarty_tpl->__("first_name");?>
:</label>
        <span class="ty-control-group__item" id="first_name_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--first_name_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['last_name']) {?>
    <div class="ty-control-group cm-reload" id="last_name_update">
        <label class="ty-control-group__label" id="last_name"><?php echo $_smarty_tpl->__("last_name");?>
:</label>
        <span class="ty-control-group__item" id="last_name_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--last_name_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['function']) {?>
    <div class="ty-control-group cm-reload" id="function_update">
        <label class="ty-control-group__label" id="function"><?php echo $_smarty_tpl->__("function");?>
:</label>
        <span class="ty-control-group__item" id="function_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['function'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--function_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['email']) {?>
    <div class="ty-control-group cm-reload" id="email_update">
        <label class="ty-control-group__label" id="email"><?php echo $_smarty_tpl->__("email");?>
:</label>
        <span class="ty-control-group__item" id="email_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['email'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--email_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['description']) {?>
    <div class="ty-control-group cm-reload" id="description_update">
        <label class="ty-control-group__label" id="description"><?php echo $_smarty_tpl->__("description");?>
:</label>
        <span class="ty-control-group__item" id="description_value"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['staff']->value['description'],25000,"...",true);?>
</span>
    <!--description_update--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['staff']->value['position']) {?>
    <div class="ty-control-group cm-reload" id="position_update">
        <label class="ty-control-group__label" id="position"><?php echo $_smarty_tpl->__("position");?>
:</label>
        <span class="ty-control-group__item" id="position_value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff']->value['position'], ENT_QUOTES, 'UTF-8');?>
</span>
    <!--position_update--></div>
<?php }?>

<?php }?><?php }} ?>
