<?php /* Smarty version Smarty-3.1.21, created on 2016-01-15 11:12:45
         compiled from "/var/www/html/cscart_standart/design/backend/templates/views/settings/trial_expired.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18615280465698a9fdbfc3b2-39510086%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ef8f38e8dad7059f283d1119d1ac55fcf9b8f6f' => 
    array (
      0 => '/var/www/html/cscart_standart/design/backend/templates/views/settings/trial_expired.tpl',
      1 => 1442295492,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18615280465698a9fdbfc3b2-39510086',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'show' => 0,
    'store_mode_errors' => 0,
    'message' => 0,
    'config' => 0,
    'store_mode_license' => 0,
    'buy_license' => 0,
    'ldelim' => 0,
    'rdelim' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5698a9fdc5ab47_97363133',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5698a9fdc5ab47_97363133')) {function content_5698a9fdc5ab47_97363133($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/var/www/html/cscart_standart/app/functions/smarty_plugins/block.inline_script.php';
?><?php
fn_preload_lang_vars(array('trial_expired','text_input_license_code','please_enter_license_here','activate','buy_license','text_buy_new_license'));
?>
<?php if ($_smarty_tpl->tpl_vars['show']->value) {?>
    <a id="trial" class="cm-dialog-opener cm-dialog-auto-size hidden cm-dialog-non-closable" data-ca-target-id="trial_dialog"></a>
<?php }?>

<div class="hidden trial-expired-dialog" title="<?php echo $_smarty_tpl->__("trial_expired",array("[product]"=>@constant('PRODUCT_NAME')));?>
" id="trial_dialog">
    <?php if ($_smarty_tpl->tpl_vars['store_mode_errors']->value) {?>
        <div class="alert alert-error notification-content">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['store_mode_errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
            <strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['title'], ENT_QUOTES, 'UTF-8');?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['message']->value['text'];?>
<br>
        <?php } ?>
        </div>
    <?php }?>

    <form name="trial_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
">
        <input type="hidden" name="store_mode" value="full">
        <div  style="padding: 0 10px;" class="trial-expired">
            <p style="margin: 20px 0;"><?php echo $_smarty_tpl->__("text_input_license_code",array("[product]"=>@constant('PRODUCT_NAME')));?>
</p>

            <div style="text-align: center;" class="license <?php if ($_smarty_tpl->tpl_vars['store_mode_errors']->value) {?> type-error<?php }?> item">
                    <input type="text" name="license_number" class="<?php if ($_smarty_tpl->tpl_vars['store_mode_errors']->value) {?> type-error<?php }?>" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['store_mode_license']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo $_smarty_tpl->__("please_enter_license_here");?>
">
                    <input name="dispatch[settings.change_store_mode]" type="submit" value="<?php echo $_smarty_tpl->__("activate");?>
" class="btn btn-primary">
            </div>

            <?php if (fn_allowed_for("ULTIMATE")) {?>
                <?php $_smarty_tpl->tpl_vars["buy_link"] = new Smarty_variable("http://www.cs-cart.ru/cs-cart-rus-pack.html?utm_source=trial", null, 0);?>
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars["buy_link"] = new Smarty_variable("http://www.cs-cart.ru/multi-vendor-rus-pack.html?utm_source=trial", null, 0);?>
            <?php }?>

            <?php ob_start();
echo $_smarty_tpl->__("buy_license");
$_tmp5=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["buy_license"] = new Smarty_variable("<p style=\"text-align: center;\"><a class=\"btn btn-warning btn-large btn-buy\" style=\"color: #fff; font-weight: bold\" target=\"_blank\" href=\"".((string)$_smarty_tpl->tpl_vars['buy_link']->value)."\">".$_tmp5."</a></p>", null, 0);?>

            <p style="margin: 20px 0;"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['buy_license']->value;?>
<?php $_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->__("text_buy_new_license",array("[buy_license]"=>$_tmp6));?>
</p>
        </div>
    </form>
</div>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
Tygh.$(document).ready(function()<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>

    <?php if ($_smarty_tpl->tpl_vars['show']->value) {?>
        Tygh.$('#trial').trigger('click');
    <?php }?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
);
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php }} ?>
