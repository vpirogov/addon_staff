<?php /* Smarty version Smarty-3.1.21, created on 2016-01-18 12:29:46
         compiled from "/var/www/html/cscart_standart/design/backend/templates/addons/staff/views/staff/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19028202085698aefe5afec6-42746294%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f55bc5800d237f86228aba85e151c2b0145e4fe9' => 
    array (
      0 => '/var/www/html/cscart_standart/design/backend/templates/addons/staff/views/staff/update.tpl',
      1 => 1453109384,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19028202085698aefe5afec6-42746294',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5698aefe66d4b0_79401132',
  'variables' => 
  array (
    'staff_member' => 0,
    'allow_save' => 0,
    'id' => 0,
    'b_type' => 0,
    'hide_first_button' => 0,
    'hide_second_button' => 0,
    'title' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5698aefe66d4b0_79401132')) {function content_5698aefe66d4b0_79401132($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('first_name','last_name','function','description','image','email','position','staff.new_staff_member','staff.editing_staff_member'));
?>
<?php if ($_smarty_tpl->tpl_vars['staff_member']->value) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['staff_member']->value['staff_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable(0, null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["allow_save"] = new Smarty_variable(fn_allow_save_object($_smarty_tpl->tpl_vars['staff_member']->value,"staff"), null, 0);?>



<?php $_smarty_tpl->tpl_vars["b_type"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['staff_member']->value['type'])===null||$tmp==='' ? "G" : $tmp), null, 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit  <?php if (!$_smarty_tpl->tpl_vars['allow_save']->value) {?> cm-hide-inputs<?php }?>" name="staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="staff_member_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />

<?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
<div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_first_name" class="control-label cm-required"><?php echo $_smarty_tpl->__("first_name");?>
</label>
        <div class="controls">
        <input type="text" name="staff_member_data[first_name]" id="elm_staff_member_first_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
" size="5"/></div>
    </div>       

    <div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_last_name" class="control-label cm-required"><?php echo $_smarty_tpl->__("last_name");?>
</label>
        <div class="controls">
        <input type="text" name="staff_member_data[last_name]" id="elm_staff_member_last_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
" size="5"/></div>
    </div>

    <div id="content_general">
    <div class="control-group">
        <label for="elm_staff_member_function" class="control-label cm-required"><?php echo $_smarty_tpl->__("function");?>
</label>
        <div class="controls">
        <input type="text" name="staff_member_data[function]" id="elm_staff_member_function" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['function'], ENT_QUOTES, 'UTF-8');?>
" size="5"/></div>
    </div>

    <div class="control-group cm-no-hide-input">
    <label for="elm_staff_member_description" class="control-label"><?php echo $_smarty_tpl->__("description");?>
</label>
        <div class="controls">
        <textarea name="staff_member_data[description]" id="elm_staff_member_description" cols="25" rows="8" class="cm-wysiwyg input-large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
        </div>
    </div>

    <div class="control-group <?php if ($_smarty_tpl->tpl_vars['b_type']->value!="G") {?>hidden<?php }?>" id="staff_member_graphic">
        <label class="control-label"><?php echo $_smarty_tpl->__("image");?>
</label>
        <div class="controls">
            <?php echo $_smarty_tpl->getSubTemplate ("common/attach_images.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_name'=>"staff_main",'image_object_type'=>"promo",'image_pair'=>$_smarty_tpl->tpl_vars['staff_member']->value['main_pair'],'image_object_id'=>$_smarty_tpl->tpl_vars['id']->value,'no_detailed'=>true,'hide_titles'=>true,'hide_alt'=>true), 0);?>

        </div>
    </div>        

    <div class="control-group">
        <label for="elm_staff_member_email" class="control-label cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
        <div class="controls">
            <input type="text" name="staff_member_data[email]" id="elm_staff_member_email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['email'], ENT_QUOTES, 'UTF-8');?>
" size="3"/>
        </div>
    </div>

    <div class="control-group">
        <label for="elm_staff_member_position" class="control-label"><?php echo $_smarty_tpl->__("position");?>
</label>
        <div class="controls">
            <input type="number" name="staff_member_data[position]" id="elm_staff_member_position" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['staff_member']->value['position'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
" size="5"/>
        </div>
    </div>  

    <?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"staff_member_data[status]",'id'=>"elm_staff_member_status",'obj_id'=>$_smarty_tpl->tpl_vars['id']->value,'obj'=>$_smarty_tpl->tpl_vars['staff_member']->value,'hidden'=>true), 0);?>

    
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'active_tab'=>$_REQUEST['selected_section'],'track'=>true), 0);?>


<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit-link",'but_target_form'=>"staff_form",'but_name'=>"dispatch[staff.update]"), 0);?>

    <?php } else { ?>
        <?php if (fn_allowed_for("ULTIMATE")&&!$_smarty_tpl->tpl_vars['allow_save']->value) {?>
            <?php $_smarty_tpl->tpl_vars["hide_first_button"] = new Smarty_variable(true, null, 0);?>
            <?php $_smarty_tpl->tpl_vars["hide_second_button"] = new Smarty_variable(true, null, 0);?>
        <?php }?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[staff.update]",'but_role'=>"submit-link",'but_target_form'=>"staff_form",'hide_first_button'=>$_smarty_tpl->tpl_vars['hide_first_button']->value,'hide_second_button'=>$_smarty_tpl->tpl_vars['hide_second_button']->value,'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

    <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
</form>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_smarty_tpl->__("staff.new_staff_member"), null, 0);?>
<?php } else { ?>
    <?php ob_start();
echo $_smarty_tpl->__("staff.editing_staff_member");
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_tmp1.": ".((string)$_smarty_tpl->tpl_vars['staff_member']->value['first_name'])." ".((string)$_smarty_tpl->tpl_vars['staff_member']->value['last_name']), null, 0);?>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['title']->value,'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>



<?php }} ?>
