<?php /* Smarty version Smarty-3.1.21, created on 2016-01-15 11:34:03
         compiled from "/var/www/html/cscart_standart/design/backend/templates/addons/staff/views/staff/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11614888425698aefb263234-45850025%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fe90a3bf2f74b3977b4cada838d68ece7ffd1fb' => 
    array (
      0 => '/var/www/html/cscart_standart/design/backend/templates/addons/staff/views/staff/manage.tpl',
      1 => 1452776971,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11614888425698aefb263234-45850025',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'staff' => 0,
    'staff_member' => 0,
    'no_hide_input' => 0,
    'id' => 0,
    'input_name' => 0,
    'obj_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5698aefb31e748_26288017',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5698aefb31e748_26288017')) {function content_5698aefb31e748_26288017($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('position','staff_name','status','edit','delete','active','hidden','deactive','no_data','add_staff_member','staff'));
?>



<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="staff_form" class=" " enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />

<?php if ($_smarty_tpl->tpl_vars['staff']->value) {?>
<table class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">
    <?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
    <th width="10%" ><?php echo $_smarty_tpl->__("position");?>
</th>
    <th width="40%" ><?php echo $_smarty_tpl->__("staff_name");?>
</th>    
    <th width="6%">&nbsp;</th>
    <th width="10%" class="right"><?php echo $_smarty_tpl->__("status");?>
</th>
</tr>
</thead>
<?php  $_smarty_tpl->tpl_vars['staff_member'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['staff_member']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['staff']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['staff_member']->key => $_smarty_tpl->tpl_vars['staff_member']->value) {
$_smarty_tpl->tpl_vars['staff_member']->_loop = true;
?>

<tr class="cm-row-status-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['staff_member']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
  
    <td class="left">
        <input type="checkbox" name="staff_member_ids[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['staff_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-item "/>
    </td>
    <td class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input']->value, ENT_QUOTES, 'UTF-8');?>
">
        <a class="row-status" href="<?php echo htmlspecialchars(fn_url("staff.update?staff_member_id=".((string)$_smarty_tpl->tpl_vars['staff_member']->value['staff_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['staff_member']->value['position'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
</a>
    </td>    
    <td class="row-status"><?php if ($_smarty_tpl->tpl_vars['staff_member']->value['first_name']||$_smarty_tpl->tpl_vars['staff_member']->value['last_name']) {?><a href="<?php echo htmlspecialchars(fn_url("staff.update?staff_member_id=".((string)$_smarty_tpl->tpl_vars['staff_member']->value['staff_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
</a><?php } else { ?>-<?php }?></td>    
    <td>
        <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
            <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>__("edit"),'href'=>"staff.update?staff_member_id=".((string)$_smarty_tpl->tpl_vars['staff_member']->value['staff_id'])));?>
</li>
            <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-confirm cm-post",'text'=>__("delete"),'href'=>"staff.delete?staff_member_id=".((string)$_smarty_tpl->tpl_vars['staff_member']->value['staff_id'])));?>
</li>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <div class="hidden-tools">
            <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

        </div>
    </td>    

    <td class="right">
        <?php if ($_smarty_tpl->tpl_vars['staff_member']->value['status']=="A") {?>
            <label class="radio inline" for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['status'], ENT_QUOTES, 'UTF-8');?>
_p"><input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_p" checked="checked" value="A"/><?php echo $_smarty_tpl->__("active");?>
</label>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['staff_member']->value['status']=="H") {?>
            <label class="radio inline" for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['status'], ENT_QUOTES, 'UTF-8');?>
_p"><input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_p" checked="checked" value="H"/><?php echo $_smarty_tpl->__("hidden");?>
</label>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['staff_member']->value['status']=="D") {?>
            <label class="radio inline" for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['staff_member']->value['status'], ENT_QUOTES, 'UTF-8');?>
_p"><input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8');?>
" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_p" checked="checked" value="D"/><?php echo $_smarty_tpl->__("deactive");?>
</label>
        <?php }?>
        
    </td>
</tr>
<?php } ?>
</table>
<?php } else { ?>
    <p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("main_tools_list", null, null); ob_start(); ?>
        <?php if ($_smarty_tpl->tpl_vars['staff']->value) {?>
            <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[staff.m_delete]",'form'=>"staff_form"));?>
</li>
        <?php }?>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['main_tools_list']));?>
    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"staff.add",'prefix'=>"top",'hide_tools'=>"true",'title'=>__("add_staff_member"),'icon'=>"icon-plus"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("staff"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'select_languages'=>true), 0);?>



<?php }} ?>
